#ifndef _MAVG_H_
#define _MAVG_H_

//--------------------------------------------------------------------------------------------------
// TYPEDEFS and ENUMS
//--------------------------------------------------------------------------------------------------

#define SMA_FILTER_LENGTH 64 //Length of Simple Moving Average (SMA)

//Define the Simple Moving Average (SMA) data structure
typedef struct {

	uint16_t buffer[SMA_FILTER_LENGTH];	//the array of measurements
	uint16_t head;      //index to write next value (new value)
    uint16_t count;     //Used to calculate average until buff is full
	uint16_t average;   //the moving average value, will be 0 until array is full
	uint32_t total;      //sum of the the measured values
    uint8_t  full;      //Flag indicating buffer is full
    
        
} SMA_DATA_T;

//--------------------------------------------------------------------------------------------------
// VARIABLES USED EXTERNALLY
//--------------------------------------------------------------------------------------------------
 
//--------------------------------------------------------------------------------------------------
// FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------

void smaInit(SMA_DATA_T *avg_data);
uint16_t smaAdd (SMA_DATA_T *avg_data,uint16_t new_value);
uint8_t smaBufferFull(SMA_DATA_T *avg_data);


#endif // _MAVG_H_




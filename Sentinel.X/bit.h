/* 
 * File:   bit.h
 * Author: Dave
 *
 * Created on July 15, 2021, 8:01 PM
 */

#ifndef BIT_H
#define	BIT_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SW1    PORTAbits.RA1
#define BITin0 PORTBbits.RB7  
#define BITin1 PORTBbits.RB6
#define BITin2 PORTBbits.RB5
#define BITin3 PORTAbits.RA4
#define BITin4 PORTBbits.RB4
#define BITin5 PORTAbits.RA3
#define BITin6 PORTAbits.RA2
#define BITin7 

#define BITout0 PORTBbits.RB8    
#define BITout1 PORTBbits.RB9
#define BITout2 PORTAbits.RA7
#define BITout3 PORTAbits.RA6
#define BITout4 PORTBbits.RB10
#define BITout5 PORTBbits.RB11
#define BITout6 PORTBbits.RB12
#define BITout7 
    
//Function prototypes    
uint8_t read_BITcal(void);
void write_BITout(uint8_t a);

#ifdef	__cplusplus
}
#endif

#endif	/* BIT_H */


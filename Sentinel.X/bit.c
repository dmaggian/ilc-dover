//**************************************************************************************************
//**************************************************************************************************
//
// FILENAME:  bit.c   
//
// PROJECT:  ILC Dover Sentinel
//
// DESCRIPTION:  Functions to read BITcal values and write BITout values
//
// VERSION:  1.00
//
// TARGET DEVICE:  MICROCHIP PIC24F32KA302, 28-pin
//
// PROGRAMMED PART NO:  TBD
//
// D. Maggiano, 07/30/2021
// 
//
//**************************************************************************************************
//**************************************************************************************************
#include "mcc_generated_files/system.h"
#include "bit.h"


 // Function to write non contiguous BITout bits as a register
// Returns the bits as a uint8_t

void write_BITout(uint8_t a) 
 { 
   BITout0 =  a & 0x1;
   BITout1 = (a & 0x2) != 0; 
   BITout2 = (a & 0x4) != 0;
   BITout3 = (a & 0x8) != 0;
   BITout4 = (a & 0x10) != 0;
   BITout5 = (a & 0x20) != 0;
   BITout6 = (a & 0x40) != 0;

 }

// Function to read non contiguous BITcal inputs as a register. 
// Returns the bits as a uint8_t

uint8_t read_BITcal(void) {
    uint8_t a = 0;
    
    //Read Bits from MSB to LSB
    if(0 == BITin6) { a &= ~(1 << 6); } else { a |= 1 << 6; }
    if(0 == BITin5) { a &= ~(1 << 5); } else { a |= 1 << 5; }
    if(0 == BITin4) { a &= ~(1 << 4); } else { a |= 1 << 4; }
    if(0 == BITin3) { a &= ~(1 << 3); } else { a |= 1 << 3; }
    if(0 == BITin2) { a &= ~(1 << 2); } else { a |= 1 << 2; }
    if(0 == BITin1) { a &= ~(1 << 1); } else { a |= 1 << 1; }
//    if(0 == BITin0) { a &= 1; } else { a |= 1; }

    return a;
}    


//**************************************************************************************************
//**************************************************************************************************
//
// FILENAME:  mvagc.c   
//
// PROJECT:  ILC Dover Sentinel
//
// DESCRIPTION: Simple moving average filter. This program maintains a 
//              circular buffer of values. Each time a new value is added
//              the oldest value is subtracted and a new average is derived
//              by dividing the total by the number of values. This is 
//              intended to be used with integer values such as raw adc
//              values so cumulative errors should not be a problem.
//
// VERSION:  1.00
//
// TARGET DEVICE:  MICROCHIP PIC24F32KA302, 28-pin
//
// PROGRAMMED PART NO:  TBD
//
// D. Maggiano, 07/30/2021
// 
//
//**************************************************************************************************
//**************************************************************************************************

#include <stdint.h>
#include "mavg.h"

//Module Variables.

// Initialize the moving average parameters
void smaInit(SMA_DATA_T *sma_data) {
	unsigned char i;

	// Zero out current measure variables
	for (i = 0; i < SMA_FILTER_LENGTH; i++) {
		sma_data->buffer[i] = 0;
	}
    sma_data->count = 0;
	sma_data->average = 0;
	sma_data->head = 0;
	sma_data->total = 0;	
}

// Maintain a circular array of values that is SMA_FILTER_LENGTH long. Each 
// time a new value is added to the total the old value is subtracted and
// the new average is calculated by dividing the total by the count.  

uint16_t smaAdd (SMA_DATA_T *sma_data,uint16_t new_value) {

    //Always add the new value to the total
    sma_data->total += new_value;
    
    //If the buffer is full that means we'll be overwriting the old value 
    //so subtract it from the total first. 
    if(sma_data->full) {
        sma_data->total -= sma_data->buffer[sma_data->head];        
    }

    //Add the new value and increment the index
    sma_data->buffer[sma_data->head] = new_value;
    sma_data->head++;
    
    //If the buffer is full add the new value to the buffer and 
    //adjust indexes if necessary
    if(sma_data->head == SMA_FILTER_LENGTH ) {
        sma_data->head = 0;
        sma_data->full = 1;
    } 
    
    //Calculate the average and return it. If the buffer isn't full divide
    //total by count else divide total by SMA_FILTER_LENGTH
    if(!sma_data->full) {
        sma_data->count++;
        sma_data->average = sma_data->total / sma_data->count;
    }else {
        sma_data->average = sma_data->total / SMA_FILTER_LENGTH;      
    }
    return (sma_data->average);
    
}

uint8_t smaBufferFull(SMA_DATA_T *sma_data) {

    return(sma_data->full);

}


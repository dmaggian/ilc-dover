/*
 File: ms5607.c
 * Description:
 *      This file contains helper functions to read temperature and pressure
 *      values along with the calibration values stored in prom that are
 *      used to compensate the measurements
 */

#include <stdio.h>
#include "mcc_generated_files/mcc.h"
#include "ms5607.h"
#include "math.h"

//#define FCY 4000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>

// Variable for this module)

uint32_t D1,D2 = 0x12345;
double T, T2, P, SENS, SENS2, OFF, OFF2, dT;

union ArrayToInteger {
  uint8_t array[4];
  uint32_t integer;
};

uint8_t buf[20];
uint16_t reg_val;
uint16_t c[8];

void ms5607_reset(void) {
    I2C2_MESSAGE_STATUS pstatus;
    
    //Call the I2C wrapper function to send the reset command
    ms5607_write_1byte(MS5607_ADDRESS, MS5607_RESET, &pstatus );
            
    __delay_ms(10);
}

//Wrapper function to read the prom calibration values
void ms5607_read_prom(void) {

    I2C2_MESSAGE_STATUS pstatus;
    
    //Call the I2C wrapper function to send the read prom command
    
    //Now read the 8 calibration values into an array for later use
    c[0] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x00 );
    c[1] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x02 );
    c[2] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x04 );
    c[3] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x06 );
    c[4] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x08 );
    c[5] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0a );
    c[6] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0c );
    c[7] = ms5607_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0e );
  
    __delay_ms(1);

}

double ms5607_read_temperature(uint8_t cmd) {

    //Trigger the conversion
    I2C2_MESSAGE_STATUS pstatus;
    
    //Trigger the conversion, cmd specifies the oversampling ratio
    ms5607_write_1byte(MS5607_ADDRESS, cmd, &pstatus );

    //Wait the appropriate delay time based on the over sampling rate
    switch (cmd)
    {
        case MS5607_ConvertD2_256 :  __delay_ms(1);
            break;
        case MS5607_ConvertD2_512 :  __delay_ms(3);
            break;
        case MS5607_ConvertD2_1024 : __delay_ms(4);
            break;
        case MS5607_ConvertD2_2048 : __delay_ms(6);
            break;
        case MS5607_ConvertD2_4096 : __delay_ms(10);
            break;
        default:
        break;
  }    
    //Now send the read adc command and read 3 bytes   
    ms5607_write_1byte(MS5607_ADDRESS, MS5607_ADC_Read, &pstatus );
    __delay_ms(1);
    
    ms5607_read_Nbytes(MS5607_ADDRESS, &buf[1], 3, &pstatus);
    __delay_ms(1);
        
    //printf("buff[0] = %x\r\n",buf[1]);
    //printf("buff[1] = %x\r\n",buf[2]);
    //printf("buff[3] = %x\r\n",buf[3]);
    //D2 = 0x7b4144;

    //Build long int from the bytes read
    D2 = (uint32_t) 0 << 24;
    D2 |=  (uint32_t) buf[1] << 16;
    D2 |= (uint32_t) buf[2] << 8;
    D2 |= (uint32_t) buf[3];    
    
    //D2 = 0x7b4144;

    // calculate 1st order temperature conversion according to data sheet
    dT= D2 - c[5] * pow(2,8);
    T=(2000+(dT*c[6]) / pow(2,23))/100;
    
    // perform higher order correction if temperature < 20 degrees c
	//double T2=0., OFF2=0., SENS2=0.;
	if(T<2000) {
	  T2 = dT * dT / pow(2,31);
	  OFF2 = 61 * ((T-2000) * (T-2000)) / pow(2,4);
	  SENS2 = 2 * ((T-2000) * (T-2000));
	  if(T < -1500) {
	    OFF2 = OFF2 + 15 * ((T + 1500) * (T + 1500));
	    SENS2 = SENS2 + 8 * ((T +1500) * (T +1500));
	  }
	}
	
    return T;
 
}

double ms5607_read_pressure(uint8_t cmd) {

    //Trigger the conversion
    I2C2_MESSAGE_STATUS pstatus;
    
    //Call the I2C wrapper function to send the read pressure command
    ms5607_write_1byte(MS5607_ADDRESS, cmd, &pstatus );
    
    //Wait the appropriate delay time based on the over sampling rate
    switch (cmd)
    {
        case MS5607_ConvertD1_256 :  __delay_ms(1);
            break;
        case MS5607_ConvertD1_512 :  __delay_ms(3);
            break;
        case MS5607_ConvertD1_1024 : __delay_ms(4);
            break;
        case MS5607_ConvertD1_2048 : __delay_ms(6);
            break;
        case MS5607_ConvertD1_4096 : __delay_ms(10);
            break;
        default:
        break;
  }    

    //Now send the read adc command and read 3 bytes of data    
    ms5607_write_1byte(MS5607_ADDRESS, MS5607_ADC_Read, &pstatus );
    __delay_ms(1);

    ms5607_read_Nbytes(MS5607_ADDRESS, &buf[1], 3, &pstatus);
    __delay_ms(1);
    
    //printf("buff[0] = %x\r\n",buf[1]);
    //printf("buff[1] = %x\r\n",buf[2]);
    //printf("buff[3] = %x\r\n",buf[3]);

    //Build long int from the bytes read
    D1 =  (uint32_t) 0 << 24;
    D1 |= (uint32_t) buf[1] << 16;
    D1 |= (uint32_t) buf[2] << 8;
    D1 |= (uint32_t) buf[3]; 

    //P = 0;

    OFF=c[2]*pow(2,17)+dT*c[4]/pow(2,6);
    SENS=c[1]*pow(2,16)+dT*c[3]/pow(2,7);
    P=(((D1*SENS)/pow(2,21)-OFF)/pow(2,15))/100; 

    return P;
}

void ms5607_write_1byte(uint16_t address, uint8_t data, I2C2_MESSAGE_STATUS *pstatus) {
    
        // Add your application code
        #define SLAVE_I2C_GENERIC_RETRY_MAX           100
        #define SLAVE_I2C_GENERIC_DEVICE_TIMEOUT      50   // define slave timeout 
 
        uint8_t         writeBuffer[3];
        uint16_t        timeOut, slaveTimeOut;

        I2C2_MESSAGE_STATUS status = I2C2_MESSAGE_PENDING;

            // build the write buffer first
            writeBuffer[0] = data;

            // The slave device may be slow. As a work around on these 
            // slaves, the application can retry sending the transaction
            timeOut = 0;
            slaveTimeOut = 0;
 
            while(status != I2C2_MESSAGE_FAIL)
            {
                // write one byte)
                I2C2_MasterWrite(writeBuffer,1,MS5607_ADDRESS,&status);

                // wait for the message to be sent or status has changed.
                while(status == I2C2_MESSAGE_PENDING)
                {
                    // add some delay here if needed

                    // timeout checking
                    // check for max retry and skip this byte
                    if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
                        break;
                    else
                        slaveTimeOut++;
                } 
                if ((slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT) || 
                    (status == I2C2_MESSAGE_COMPLETE))
                    break;

                // if status is  I2C2_MESSAGE_ADDRESS_NO_ACK or I2C2_DATA_NO_ACK,
                // The device may be busy and needs more time for the last
                // write so we can retry writing the data, this is why we
                // use a while loop here

                // check for max retry and skip this byte, some more thought
                // needs to be given to this error scenario
                if (timeOut == SLAVE_I2C_GENERIC_RETRY_MAX)
                    break;
                else
                    timeOut++;
            }

            *pstatus = status;

}

uint8_t ms5607_read_Nbytes(uint16_t address, uint8_t *pdata, uint16_t count, I2C2_MESSAGE_STATUS *pstatus) {
    
    #define MS5607_RETRY_MAX       100  // define the retry count
    #define MS5607_DEVICE_TIMEOUT  50   // define slave timeout 


            I2C2_MESSAGE_STATUS status;
                uint16_t    retryTimeOut, slaveTimeOut;
                uint8_t     *pD;

                pD = pdata;

                 // this portion will read the byte from the memory location.

                        status = I2C2_MESSAGE_PENDING;

                        retryTimeOut = 0;
                        slaveTimeOut = 0;

                        while(status != I2C2_MESSAGE_FAIL)
                        {
                            // Read count bytes from the sensor
                            I2C2_MasterRead(pD,count,MS5607_ADDRESS,&status);

                            // wait for the message to be sent or status has changed.
                            while(status == I2C2_MESSAGE_PENDING)
                            {
                                // add some delay here

                                // timeout checking
                                // check for max retry and skip this byte
                                if (slaveTimeOut == MS5607_DEVICE_TIMEOUT)
                                    return (0);
                                else
                                    slaveTimeOut++;
                            }

                            if (status == I2C2_MESSAGE_COMPLETE)
                                break;

                            // if status is  I2C2_MESSAGE_ADDRESS_NO_ACK,
                            //               or I2C2_DATA_NO_ACK,
                            // The device may be busy and needs more time for the last
                            // write so we can retry writing the data, this is why we
                            // use a while loop here

                            // check for max retry and skip this byte
                            if (retryTimeOut == MS5607_RETRY_MAX)
                                break;
                            else
                                retryTimeOut++;
                        }
                
//                    }

                    // exit if the last transaction failed
                    if (status == I2C2_MESSAGE_FAIL)
                    {
  //                      return(0);
  //                      break;
                    }

                return(1);

}

//Helper function to read 2 byte register 
uint16_t ms5607_read2ByteRegister(uint16_t address, uint8_t reg) {
    
uint16_t result;
I2C2_MESSAGE_STATUS status;

//Write the address and then read two bytes    
ms5607_write_1byte(MS5607_ADDRESS, reg, &status );
__delay_ms(1);
ms5607_read_Nbytes(MS5607_ADDRESS, (uint8_t *)&result, 2, &status);

//Swap the bytes before returning
return (result << 8 | result >> 8);
    
}    
/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.170.0
        Device            :  PIC24F32KA302
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.61
        MPLAB 	          :  MPLAB X v5.45
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/system.h"

#include "mcc_generated_files/i2c2.h"
#include "mcc_generated_files/pin_manager.h"
#include <stdio.h>
#include <string.h>
#include "ms5607.h"
#include "bit.h"
#include "mavg.h"

//The next two defines enable outputting data to the console port using an 
//FTDI adapter connected to RB7. These defines are mutually exclusive so 
//only one should be enabled at a time. The CONSOLE_OUTPUT will output
//various data items to the screen using escape characters for easy 
//readability. The SERIAL_OSCOPE_OUTPUT define will write the raw ADC
//value and filtered ADC value only allowing an external serial oscilloscope
//such as:
//
//to be used to graph the data. Which define you enable depends on your needs.
//Before shipping these must be commented out, the uart removed and RB7
//reassigned to the lsb of the BITcal inputs

//#define CONSOLE_OUTPUT
//#define SERIAL_OSCOPE_OUTPUT

//#define FCY 4000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>

typedef enum {
 
  STATE_STARTUP,
  STATE_CALIBRATION,
  STATE_MONITOR,
  STATE_ALARM

} SENTINEL_STATES;

//Variables for this module
uint8_t  initialized = 0;
uint16_t alarm_test_flag;
uint16_t display_motor_i_flag;
uint16_t conversion,i=0;
double   ANLin, AMPalm, AMPcmp;
double   Tin, Pin;
double   Tcmp, Pcmp;
double   BITanl, BITcmp, AMPin;
uint16_t BITprecmp;
uint8_t  sentinel_state = STATE_STARTUP;
uint8_t  BITcal, BITout, motor_i;
SMA_DATA_T mvg_avg_data;


#ifdef CONSOLE_OUTPUT
//Function prototype for screen output if CONSOLE_OUTPUT defined
void console_output(double temperature, double pressure,int adc_value );
#endif

/*
                         Main application
 */


//Setup our own callback handler for Timer 1 rather than having it in
//the tmr1.c file that may get over written with MCC config updates. This 
//callback should get called at the timer 1 rate which is 1ms.
//
//This handler will take an A/D measurement every 10ms and update the 
//moving average. It will also set a flag every 5 seconds to trigger a 
//measurement by the main app. 
void tmr1_callback(void) {
    static uint8_t adc_count = 0;
    static uint16_t i, alarm_test_count, thirty_second_count = 0;
    
    //If the program initialization is complete read ADC and update
    //moving average filter every 20ms. 
    //alarm_test_flag every 5 seconds
    if(initialized) {
        if (adc_count++ >= 20) {
            adc_count = 0;

            //Take the ADC measurement. Uncomment LED_Toggle() calls to use
            //led to see how long measurement takes

            LED_Toggle();  
            ADC1_Enable();
            ADC1_ChannelSelect(channel_AN0);
            ADC1_SoftwareTriggerEnable();
            //Provide a short Delay
            for(i=0;i <1000;i++)
            {
            }            
            ADC1_SoftwareTriggerDisable();
            while(!ADC1_IsConversionComplete(channel_AN0));
            conversion = ADC1_ConversionResultGet(channel_AN0);
            ADC1_Disable(); 
                       
            //Add raw ADC measurement to the moving average filter            
            smaAdd(&mvg_avg_data,conversion);

            //Uncomment led toggle to see the frequency of the ADC timing 
            LED_Toggle();
        }
        
        //Set a flag to tell the main app to test for an alarm condition
        //every 5 seconds
        if(alarm_test_count++ >= 5000) {
            alarm_test_count = 0;            
            alarm_test_flag = 1;
        }
        
        //Set a flag at 30 seconds that will cause the adjusted motor 
        //current to be displayed on the BITout plug
        if(!display_motor_i_flag && thirty_second_count++ >= 5000) {
            display_motor_i_flag = 1;            
        }
        
    }
}

int main(void)
{
    uint8_t flash_cnt = 0;
    uint8_t under = 0;
    
    // initialize the device
    SYSTEM_Initialize();
    
    //Setup a local callback to read ADC
    TMR1_SetInterruptHandler(tmr1_callback);

    while (1)
    {
        //Sentinel state machine
        switch(sentinel_state) {

            case STATE_STARTUP:
                LED_SetLow();
                BUZZER_SetLow();                
                __delay_ms(100);
                //Initialize the moving average filter
                smaInit(&mvg_avg_data);
                //Flash/Sound led/ buzzer 3 times for .5 seconds
                for(flash_cnt = 0;flash_cnt <= 4; flash_cnt++) {
                    BUZZER_Toggle();
                    LED_Toggle();
                    __delay_ms(500);
                }
                LED_SetHigh();
                BUZZER_SetLow();
                    
                //Reset the MS5607 and read the calibration values
                ms5607_reset();
                ms5607_read_prom();
                    
                //Initialize the adc
                ADC1_Initialize();
                initialized = 1;

                //Delay a bit to let moving average filter get full
                __delay_ms(1000);                                
                
                //If the Calibrated switch is false goto the calibration 
                //state otherwise goto the monitor state
                if(1 == SW1) {
                    sentinel_state = STATE_MONITOR;                       
                } else {
                    sentinel_state = STATE_CALIBRATION;
                }

                //Read BITcal value and copy it to J5 header
                BITcal = read_BITcal();
                write_BITout(BITcal);

                //Temporarily set AMPalm value for testing
                //AMPalm = 1100;
                break;
            case STATE_CALIBRATION:
                //In the calibration state the program will continuously 
                //output calculated 7 bit binary BITout value. The program will
                //remain in this state until the device is power cycled with
                //the calibrated switch in the on '1' position.
                                
                //Calculate the BITout value to send to the PLC over J5
                //Only send it once for now to avoid confusion with leds
                ANLin = (double)mvg_avg_data.average;
                AMPin = (ANLin * (1.5/4096) * 1000);
                //AMPcmp = AMPin * Pcmp * Tcmp;
                //BITout = AMPcmp - 360;
                BITout = AMPin;
                write_BITout(BITout);
                    
                break;
            case STATE_MONITOR:
                //This state monitors the input voltage and generates
                //an alarm if it reaches a predetermined state. The 
                //tmr1 interrupt handler sets a flag every 5 seconds to 
                //trigger a test. It also sets a flag after 5 seconds 
                //at which point we'll update the BITout plug with 
                //the adjusted motor current every cycle
                if(display_motor_i_flag) {
                    //AMPcmp = AMPin * Pcmp * Tcmp;                
                    //motor_i = (uint8_t)(AMPcmp - 360);
                    motor_i = (uint8_t)(AMPin - 360);
                    write_BITout(motor_i);
                }
                if(alarm_test_flag) {
                    alarm_test_flag = 0;
                    BITcal = read_BITcal();
                    //AMPalm = BITcal + 360 * Pcmp * Tcmp;
                    AMPalm = BITcal + 360;
                    if (AMPin < AMPalm) {
                        under++;
                        if(under >= 3) {
                            sentinel_state = STATE_ALARM;
                        }
                    } else {
                        under = 0;
                    }                        
                }                
                break;
            case STATE_ALARM:
                //This state only occurs if a low flow alarm condition 
                //happens at which point the device will flash the LED and
                //sound the buzzer every .33ms 
                //It will stay in this state until the device powered off 
                __delay_ms(330);
                BUZZER_Toggle();
                LED_Toggle();
                break;                
            default:
                //Unknown state, should never go here. Flash led
                //at 10 hz rate to indicate fatal error
                while(1) {
                    LED_Toggle();
                    __delay_ms(100);
                }
                break;
        }

        //If the ADC and MS5607 are initialized take a temperature and 
        //pressure reading every cycle regardless of the state so they are
        //available during either calibration or normal monitoring
        if(initialized) {
            
            //Take temperature and pressure readings and calculate the 
            //Tcmp and Pcmp values
            Tin =  ms5607_read_temperature(MS5607_ConvertD2_2048);
            Pin = ms5607_read_pressure(MS5607_ConvertD1_2048);            
            Tcmp = .002 * Tin + .85;
            Pcmp = .00004 * Pin + .9958;
            
            
            //Convert the averaged analog values that are being taken
            //in the background to current. The INA250 part used in this
            //board 2V/A so at 3 volts full scale is 1.5 amps
            ANLin = (double)mvg_avg_data.average;
            AMPin = (ANLin * (1.5/4096) * 1000);
            
        }

#ifdef SERIAL_OSCOPE_OUTPUT
printf("%d,%d\r", conversion,(uint16_t)mvg_avg_data.average);                
#endif
        
#ifdef CONSOLE_OUTPUT
        console_output(Tin,Pin,conversion);
#endif
        __delay_ms(100);

    }

    return 1;
}


#ifdef CONSOLE_OUTPUT
void console_output(double temperature, double pressure,int adc_value ) {

    static unsigned long cnt = 0;
    double inHg;
    double degF;
    static int screen_init = 0;
    uint8_t a;
    
    //If it's the fist time through, clear the screen
    //Clear the screen and hide the cursor
    if(!screen_init) {
        printf("\x1b[2J");  //Clear screen
        printf("\e[?25l");  //Hide cursor
        printf("\x1b[32m"); //Set text color to green
        
        screen_init = 1;
    }
    
    //Clear the screen and the current line 
    //since ADC readings vary in length
    printf("\x1b[%d;%df", 1, 10);
    printf("\x1b[1J");
    printf("\x1b[%d;%df", 1, 1);
    printf("ADC = %d, Filtered Value = %d\r\n", adc_value, (uint16_t)mvg_avg_data.average);    
    printf("AMPin = %6.2f AMPalm = %6.2f\r\n", AMPin,AMPalm);
    printf("Temp/Press conversions %lu\r\n",cnt++);
    degF = (temperature * 9 /5) + 32;
    printf("Temperature = %.2f\370 C, %.2f (F)\r\n", temperature,degF);

    //pressure = ms5607_read_press(MS5607_ConvertD1_4096);        
    inHg = pressure * 0.0295301;
    printf("Pressure = %.2f millibars, %.2f (inHg)\r\n", pressure, inHg);
    printf("Tcmp = %.2f, Pcmp = %.2f\r\n",Tcmp,Pcmp);
    
    a = read_BITcal();
    printf("BITcal = %3d ", a);

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

    printf(BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(a));        
    printf("\r\n");
    // Add your application code
    
}
#endif


/*
 End of File
*/

